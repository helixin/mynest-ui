/*基本属性配置*/
export class Setting {
  public static APP: any = {                           //平台信息
    defaultImg: '../../../assets/imgs/err-default.png',
    userDefaultImg: '../../assets/imgs/user.png'
  };

  /**
   * 数据字典key
   */
  public static DATADIRECTORY = {
    applyBusinessCodeMinLimit: 'APPLY_BUSINESS_ACTIVATION_MIN_NUM',//商务中心申请激活码最小限制
    withdrawBasicAmount: 'BASICS_QUOTA', //提现基础数额
    reviveCycle:'REVIVE_CYCLE',//复投期时间限制
  }

  //定义枚举
  static ENUM: any = {
    yesOrNo: '1001',    //是否
    tradeWay: '2001',   //交易方式
    withdrawState: '2002',//提现状态
    codeApplyState: '2003',//激活码申请状态
    userRoleState: '2004',//用户角色状态
    userState: '2005',      //用户状态
    businessState: '2006',//商务中心状态
  };

  static ENUMSTATE: any = {
    yes: 'Y',
    no: 'N',
    withdrawState: {
      done: 'DONE',//提现到账
      deal: 'DEAL',//提现处理中
      fail: 'FAIL',//提现失败
      cr: 'CR'//提现失败
    },
    codeApplyState: {
      pass: 'PASS',// 已发放
      apply: 'APPLY'// 申请中
    },
    userRoleState: {
      activate: 'ACTIVATE',//已激活
      out: 'OUT'//已出局
    },
    userState: {
      abnormal: 'ABNORMAL',//不正常
      normal: 'NORMAL'//正常
    },
    businessState: {
      no: 'NO',//未开通
      apply: 'APPLY',//申请中
      open: 'OPEN'//已开通
    }
  }

  constructor() {
  }

}
