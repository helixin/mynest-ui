/*接口访问路径配置*/

export class SettingUrl {
  static proxyUrl:string = "";  //全局代理地址
  // static proxyUrl:string = "";  //全局代理地址
  static URL: any = {
    /**
     * 基础路径配置
     */
    base: {
      enum: SettingUrl.proxyUrl + '/res/enum/',                             //获取枚举接口
      dataDictionary: SettingUrl.proxyUrl + '/admin/basicsettings/loadByK', //数据字典
    },
    /**
     * 用户中心
     */
    center: {
      getUserInfo: SettingUrl.proxyUrl + '/user/load/id',                    //获取用户信息
      getUserTicketAccount: SettingUrl.proxyUrl + '/useraccount/load',      //获取用户代金券账户信息
      getUserFund: SettingUrl.proxyUrl + '/useraccount/load/fund',          //获取用户基金
      getUserBalance: SettingUrl.proxyUrl + '/useraccount/loadBalance',    //获取用户余额
      getUserJournal: SettingUrl.proxyUrl + '/useraccount/list',            //获取用户账户流水
      getUserJournalDetail: SettingUrl.proxyUrl + '/useraccount/load/rec', //获取用户某一流水明细
      applyCode: SettingUrl.proxyUrl + '/useractivationapply/build',        //申请激活码
      countFeeWhenApply: SettingUrl.proxyUrl + '/useractivationapply/buildFee',//申请激活码时计算费用
      applyBusinessCode: SettingUrl.proxyUrl + '/useractivationapply/build',  //申请激活码
      insertOrder: SettingUrl.proxyUrl + '/orders/insert',                      //提交购物申请
      withdraw: SettingUrl.proxyUrl + '/userdrawrec/build',                   //提交提现申请
      getWithDrawFee: SettingUrl.proxyUrl + '/userdrawrec/calFee',           //计算提现手续费
      getApplyList: SettingUrl.proxyUrl + '/useractivationapply/list',       //获取申请的激活码记录列表
      getApplyCodesList: SettingUrl.proxyUrl + '/useractivationcode/list',//获取申请的激活码列表
      logout: SettingUrl.proxyUrl + '/user/logout'                            //退出登录
    },
    /**
     * 用户模块
     */
    user: {
      login: SettingUrl.proxyUrl + '/login',  //用户登录
      build: SettingUrl.proxyUrl + '/user/build',  //用户注册
      queryList: SettingUrl.proxyUrl + '/user/queryChildrenUser', //获取用户下级
      update: SettingUrl.proxyUrl + '/user/updatePwd',
      loadByName: SettingUrl.proxyUrl + '/user/loadNameByLoginAccount'
    },

    /**
     * 商品模块
     */
    goods: {
      list: SettingUrl.proxyUrl + '/goods/list',  //商品列表
    },

    /**
     * 购买记录模块
     */
    goodsbuyrecord:{
      list: SettingUrl.proxyUrl + '/goodsbuyrecord/list',  //商品购买记录列表
      buy: SettingUrl.proxyUrl + '/goodsbuyrecord/insertRecast'  //礼包复投
    },

    /**
     * 代金券模块
     */
    coupon: {
      list: SettingUrl.proxyUrl + '/coupon/list',      //查看未释放代金券列表
      load: SettingUrl.proxyUrl + '/coupon/load/id'   // 查看已释放代金券详情
    }

  };
}
