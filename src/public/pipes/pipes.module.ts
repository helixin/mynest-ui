import {NgModule} from "@angular/core";
import {SpliceStrPipe} from "./pipes/splice-str.pipe";
import {StateNamePipe} from "./pipes/state-name.pipe";

@NgModule({
	declarations: [
    SpliceStrPipe,
    StateNamePipe
  ],
	imports: [],
	exports: [
    SpliceStrPipe,
    StateNamePipe
  ]
})
export class PipesModule {}
