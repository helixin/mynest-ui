import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'spliceStr'
})
export class SpliceStrPipe implements PipeTransform {
  /**
   * @param str
   * @param args 数字数组，第一个表示前面留取长度，第二个标识后面留取长度
   * @returns {string}截取后的字符串
   */
  transform(str: string, args: Array<number>): any {
    if (!str) return;
    let stars: string = '',
      starsLength = str.length - args[0] - args[1],
      startPlace = args[0],
      endPlace = str.length - args[1];
    for (let i = 0; i < starsLength; i++) stars += '*';
    let strr = `${str.substr(0, startPlace)} ${stars} ${str.substr(endPlace)}`;
    return strr;
  }

}
