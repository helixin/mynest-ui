import {Component, ViewChild} from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {ListPage} from "../list/list";

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: '我的家', component: HomePage },
      { title: '日志', component: ListPage }
    ];
  }

  initializeApp() {

  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.rootPage = page.component
    // this.navCtrl.push(page.component);
  }

  ceshi(){
    console.log(1);
  }

}
